# Webinar Instituto Tecnológico Sudamericano

Está plantilla se encuentra optimizada para pantallas cuya relación de aspecto es **16:9**

## Estructura

- Presentación: [webinar.pdf](webinar.pdf)
- Documento fuente de la plantilla: [webinar.tex](webinar.tex)
- Documento pdf con vista previa de la plantilla [webinar.pdf](webinar.pdf)

## Las interfaces en el desarrollo de Software

presentación creada por invitación del instituto tecnológico sudamericano de la ciudad de Loja, donde se trato sobre el diseño y el proceso para la creación de interfaces en el desarrollo de software, se tratan temas con UX, UI Accesibilidad, la teoría del color, sketch, wireframe, mockaups y prototipos