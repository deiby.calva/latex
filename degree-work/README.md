# Plantilla trabajo de titulación

Está plantilla se encuentra optimizada para trabajos de titulación de la carrera de ingeniería en sistemas de la [UNL](http://www.unl.edu.ec)

## Estructura

- Documento fuente de la plantilla: [degree-work.tex](degree-work.tex)
- Documento pdf con vista previa de la plantilla [degree-work.pdf](degree-work.pdf)

- Directorio preliminaries donde se encuentran los archivos fuentes de
	+ [authory-certification](preliminaries/authory-certification.tex): certificado de autoría
	+ [cover](preliminaries/cover.tex): Portada del trabajo de titulación
	+ [dedication](preliminaries/dedication.tex): Dedicatoria
	+ [director-certification](preliminaries/director-certification.tex): Certificación del director del trabajo de titulación
	+ [gratitude](preliminaries/gratitude.tex): Agradecimientos
	+ [letter-authorization](preliminaries/letter-authorization.tex): Carta de autorización
- Directorio sections, donde encontrará las distintas secciones del TT
	+ [a-title](sections/a-title.tex): Agradecimientos
	+ [b-abstract](sections/b-abstract.tex): Resumes
	+ [c-introduction](sections/c-introduction.tex): Introducción
	+ [d-literature-review](sections/d-literature-review.tex): Revisión de literatura
	+ [e-materials](sections/e-materials.tex): Materiales y métodos
	+ [f-results](sections/f-results.tex): Resultados
	+ [g-discussion](sections/g-discussion.tex): Discusión
	+ [h-conclusions](sections/h-conclusions.tex): Conclusiones
	+ [i-recommendations](sections/i-recommendations.tex): Recomendaciones
	+ [j-bibliography](sections/j-bibliography.tex): Bibliografía
	+ [k-annexes](sections/k-annexes.tex): Anexos
- Directorio annexes, destinado para generar los diversos anexos a utilizar en el presente TT
- Directorio bibliography
	+ [bibliography.bib](bibliography/bibliography.bib): La bibliografía del TT



- Directorio **assets**: Al interior de este encontrará los archivos multimedia utilizados en la plantilla

	- **logos**: logotipos utilizados
		- **cis.png**: Logotipo de la Carrera de Ingeniería en Sistemas/Computación  
		![cis](assets/logos/cis.png)
		![gitic](assets/logos/gitic.png)
		- **unl.png**: Logotipo de la Universidad Nacional de Loja
		![cis](assets/logos/unl.png)
		- **unl-large.png**: Logotipo de la Universidad Nacional de Loja con texto horizontal  
		![cis](assets/logos/unl-large.png)
		- **unl-vertical.png**: Logotipo de la Universidad Nacional de Loja con texto vertical  
		![cis](assets/logos/unl-vertical.png)
