# Plantilla Flisol 2021 16:9

Está plantilla se encuentra optimizada para pantallas cuya relación de aspecto es **16:9**

## Estructura

- Presentación: [flisol.pdf](flisol.pdf)
- Documento fuente de la plantilla: [flisol.tex](presentacion16x9.tex)
- Documento pdf con vista previa de la plantilla [presentacion16x9.pdf](presentacion16x9.pdf)

- Directorio **assets**: Al interior de este encontrará los archivos multimedia utilizados en la plantilla
	-  **background**: fondos utilizados en el documento  
		- **backround.png**:  
		![background](assets/background/background.png)
		- **backround-2.png**:  
		![background-2](assets/background/background-2.png)
	- **logos**: logotipos utilizados
		- **flisoloja.png**: Festival Latinoamericano de Instalación de Software Libre
		![cis](assets/logos/fliosloja.png)
		- **cis.png**: Logotipo de la Carrera de Ingeniería en Sistemas/Computación  
		![cis](assets/logos/cis.png)
		- **gitic.png**: Logotipo del Grupo de Investigación en Tecnologías de la Información y Comunicación  
		![gitic](assets/logos/gitic.png)
		- **unl.png**: Logotipo de la Universidad Nacional de Loja
		![cis](assets/logos/unl.png)
		- **unl-large.png**: Logotipo de la Universidad Nacional de Loja con texto horizontal  
		![cis](assets/logos/unl-large.png)
		- **unl-vertical.png**: Logotipo de la Universidad Nacional de Loja con texto vertical  
		![cis](assets/logos/unl-vertical.png)
