# Plantillas para propuesta de proyectos


Palntilla para informe de proyecto de investigación. Cuya estructura básica es

1. Introducción
2. Metodología
3. Resultados
4. Discusión
5. Conclusiones
6. Recomendaciones
7. Bibliografía

* Anexos

Cada una de los capítulos esta en el directorio "chapters". Los anexos estará en el directorio "appendix". El directorio bibliografia está destinado para el archivo *.bib que tiene la referencias utilizadas en el proyecto y los archivos adiconales en el directorio "assets" 
